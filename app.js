const express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  mongoose = require("mongoose"),
  flash = require("connect-flash"),
  session = require("express-session"),
  moment = require("moment"),
  passport = require("passport"),
  LocalStrategy = require("passport-local"),
  methodOverride = require("method-override");



// requiring routes
const userRoute = require("./routes/users");
const indexRoute = require("./routes/index");
const middleware = require("./middleware");

const User = require("./models/user");
const Employer = require("./models/employer");
const Admin = require("./models/admin");

//this is used to run on local server ie., locsalhost:3000
let url = process.env.DATABASEURL || "mongodb://localhost/checkin";
mongoose.connect(url, { useNewUrlParser: true });

// connect to the DB on mlab
// const databaseUri = 'mongodb://admin:admin123@ds153556.mlab.com:53556/swecha';
//  mongoose.connect(databaseUri,{ useNewUrlParser: true })
//        .then(() => console.log(`Database connected`))
//        .catch(err => console.log(`Database connection error: ${err.message}`));

app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));
app.use(flash());
app.use(bodyParser.json());
app.locals.moment = moment; // create local variable available for the application




//passport configuration
app.use(session({
  secret: 'abcd',
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use('admin',new LocalStrategy(Admin.authenticate()));
passport.use('emp',new LocalStrategy(Employer.authenticate()));
passport.use('user',new LocalStrategy(User.authenticate()));
// passport.use('student',new LocalStrategy(Team.authenticate()));
// passport.use('mentor',new LocalStrategy(Mentor.authenticate()));
// passport.use('assesment',new LocalStrategy(AssesmentUser.authenticate()));
passport.serializeUser(function(user, done) {
  done(null, user);
});



passport.deserializeUser(function(user, done) {
  if(user!=null)
    done(null,user);
});


// pass currentUser to all routes
app.use((req, res, next) => {
  res.locals.currentUser = req.user; // req.user is an authenticated user
  res.locals.error = req.flash("error");
  res.locals.success = req.flash("success");
  next();
});



// use routes
app.use("/", indexRoute);
app.use("/", userRoute);

app.listen((process.env.PORT || 3000), function () {
  console.log("The Server Has Started!4000");
});
