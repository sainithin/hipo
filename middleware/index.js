const User = require("../models/user");
const Employer = require("../models/employer");
const Admin = require("../models/admin");
// all middleware goes here
const middlewareObj = {};

middlewareObj.isLoggedIn = function (req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    req.session.redirectTo = req.originalUrl;
    req.flash("error", "You need to be logged in first"); // add a one-time message before redirect
    res.redirect("/");
};



middlewareObj.checkuserAuthentication = function (req, res, next) {
    if (req.isAuthenticated()) {
        User.findById(req.user._id, (err, foundGroup) => {
            if (err || !foundGroup) {
                req.flash("error", "You Cannot Do that Please Logout First");
                res.redirect("/");
            } else {
                return next();
            }
        });
    } else {
        res.redirect("/");
    }
};


middlewareObj.checkemployerAuthentication = function (req, res, next) {
    if (req.isAuthenticated()) {
        Employer.findById(req.user._id, (err, foundGroup) => {
            if (err || !foundGroup) {
                req.flash("error", "You Cannot Do that Please Logout First");
                res.redirect("/");
            } else {
                return next();
            }
        });
    } else {
        res.redirect("/");
    }
};


middlewareObj.checkadminAuthentication = function (req, res, next) {
    if (req.isAuthenticated()) {
        Admin.findById(req.user._id, (err, foundGroup) => {
            if (err || !foundGroup) {
                req.flash("error", "You Cannot Do that Please Logout First");
                res.redirect("/");
            } else {
                return next();
            }
        });
    } else {
        res.redirect("/");
    }
};

module.exports = middlewareObj;
