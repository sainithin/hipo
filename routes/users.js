var express = require('express');
var router = express.Router();
flash = require("connect-flash");
const path = require('path'),
  mongoose = require("mongoose"),
  session = require("express-session"),
  moment = require("moment"),
  passport = require("passport"),
   nodemailer = require("nodemailer"),
      async      = require("async"),
  LocalStrategy = require("passport-local"),
  methodOverride = require("method-override");
// import models
  const User = require("../models/user");
  const Employer = require("../models/employer");
  const Admin = require("../models/admin");


/* Employer register */
router.get('/employer_signup', function(req, res, next) {
  res.render('register/employer_signup');
});
router.get('/employer_signin', function(req, res, next) {
  res.render('login/employer_signin');
});
router.post("/employer_signup", (req, res) => {
    let newUser = new Employer({
        username: req.body.username,
        firstname: req.body.firstname,
        email:req.body.email,
    });
    Employer.register(newUser, req.body.password, (err, user) => {
        if (err) {
            if (err.email === 'MongoError' && err.code === 11000) {
                // Duplicate email
                req.flash("error", "That email has already been registered.");
                console.log(err);
                return res.redirect("/employer");
            }
            // Some other error
            req.flash("error", "Something went wrong...");
            console.log("Something wrong");
            return res.redirect("/employer");
        }

        passport.authenticate("emp")(req, res, () => {
            req.flash("success", "Welcome  " + user.username);
            res.redirect("/employer_signin");
        });
    });
});

//  employer login

router.post("/employer_signin", (req, res, next) => {
    passport.authenticate("emp", (err, user, info) => {
        if (err) {
            return next(err);
        }
        if (!user) {
            req.flash("error", "Invalid username or password");
            return res.redirect('/employer_signin');
        }
        req.logIn(user, err => {
            if (err) {
                return next(err);
            }
            let redirectTo = req.session.redirectTo ? req.session.redirectTo : ('/dashboard');
            delete req.session.redirectTo;
            req.flash("success", "Good to see you again, " + user.username);
            res.redirect(redirectTo);
        });
    })(req, res, next);
});

/* User register */
router.get('/signup', function(req, res, next) {
  res.render('register/user_signup');
});

router.post("/user_signup", (req, res) => {
    let newUser = new User({
        username: req.body.username,
        firstname: req.body.firstname,
        email:req.body.email,
    });
    User.register(newUser, req.body.password, (err, user) => {
        if (err) {
            if (err.email === 'MongoError' && err.code === 11000) {
                // Duplicate email
                req.flash("error", "That email has already been registered.");
                console.log(err);
                return res.redirect("/user_signup");
            }
            // Some other error
            req.flash("error", "Something went wrong...");
            console.log("Something wrong");
            return res.redirect("/employer");
        }

        passport.authenticate("user")(req, res, () => {
            req.flash("success", "Welcome  " + user.username);
            res.redirect("/");
        });
    });
});

router.post("/user_signin", (req, res, next) => {
    passport.authenticate("user", (err, user, info) => {
        if (err) {
            return next(err);
        }
        if (!user) {
            req.flash("error", "Invalid username or password");
            return res.redirect('/');
        }
        req.logIn(user, err => {
            if (err) {
                return next(err);
            }
            let redirectTo = req.session.redirectTo ? req.session.redirectTo : ('/home');
            delete req.session.redirectTo;
            req.flash("success", "Good to see you again, " + user.username);
            res.redirect(redirectTo);
        });
    })(req, res, next);
});

router.get("/logout", (req, res) => {
    req.logout();
    req.flash("success", "Logged out successfully. Looking forward to seeing you again!");
    res.redirect("/");
});
module.exports = router;
