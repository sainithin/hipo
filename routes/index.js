var express = require('express');
var router = express.Router();
flash = require("connect-flash");
const path = require('path'),
  mongoose = require("mongoose"),
  session = require("express-session"),
  moment = require("moment"),
  passport = require("passport"),
   nodemailer = require("nodemailer"),
      async      = require("async"),
  LocalStrategy = require("passport-local"),
  methodOverride = require("method-override");




User = require("../models/user");

const middleware = require("../middleware");


/* GET home page. */


router.get('/', function (req, res, next) {
    res.render("index");

});

router.get('/dashboard', function (req, res, next) {
    res.render("index");

});
router.get('/home', function (req, res, next) {
    res.render("./user/home");

});



module.exports = router;