const mongoose = require("mongoose"),
   passportLocalMongoose = require("passport-local-mongoose");
const EmployerSchema = new mongoose.Schema({
    username:String,
    firstname:String
});
EmployerSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model("Employer", EmployerSchema);
